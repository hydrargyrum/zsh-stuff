if [[ $REGION_ACTIVE -eq 0 ]] {
	zle backward-delete-char
} else {
	zle kill-region
}
