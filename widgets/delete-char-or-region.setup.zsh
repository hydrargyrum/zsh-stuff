autoload delete-char-or-region
zle -N delete-char-or-region
autoload backward-delete-char-or-region
zle -N backward-delete-char-or-region

# delete
bindkey "${termcap[kD]}" delete-char-or-region
# backspace
bindkey "${termcap[kb]}" backward-delete-char-or-region
# in isearch, don't use a custom widget for backspace:
# else, it seems to edit the command line instead of the isearch
bindkey -M isearch "${termcap[kb]}" backward-delete-char
