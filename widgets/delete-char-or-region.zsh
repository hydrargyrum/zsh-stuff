if [[ $REGION_ACTIVE -eq 0 ]] {
	zle delete-char
} else {
	zle kill-region
}
