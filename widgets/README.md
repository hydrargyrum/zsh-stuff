## dirname-previous-word

This widget replaces current argument with its parent directory, e.g. if current argument is /usr/share/doc/zsh, this widget will replace it with /usr/share/doc/

See it in action:

- https://asciinema.org/a/515640
- or `asciinema play dirname-previous-word.asciinema`

Setup with:

	autoload dirname-previous-word
	zle -N dirname-previous-word

	# shift-tab
	bindkey "${termcap[bt]}" dirname-previous-word


## backward-delete-char-or-region / delete-char-or-region

The default behavior for backspace/delete is to remove the previous/next character, but does not kill the selected region, as would be done in a normal text editor.
Those 2 widgets fix that behavior:
- if there's no selection, they act as usual
- if there's a selection, they kill it

See them in action:
- https://asciinema.org/a/515847
- or `asciinema play delete-char-or-region.asciinema`

Setup with:

	autoload delete-char-or-region
	zle -N delete-char-or-region
	autoload backward-delete-char-or-region
	zle -N backward-delete-char-or-region

	# delete
	bindkey "${termcap[kD]}" delete-char-or-region
	# backspace
	bindkey "${termcap[kb]}" backward-delete-char-or-region
	# in isearch, don't use a custom widget for backspace:
	# else, it seems to edit the command line instead of the isearch
	bindkey -M isearch "${termcap[kb]}" backward-delete-char
