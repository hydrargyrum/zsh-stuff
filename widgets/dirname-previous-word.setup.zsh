autoload dirname-previous-word
zle -N dirname-previous-word

# shift-tab
bindkey "${termcap[bt]}" dirname-previous-word
